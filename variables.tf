variable "gcp_project_id" {
    type = string
}

variable "gcp_zone" {
    default = "europe-west4-a"
}

variable "gcp_cluster_name" {
    default = "standard-cluster-2"
}
