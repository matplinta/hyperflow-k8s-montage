# HyperFlow k8s Montage

This is example of running 3 montage with _HyperFlow_ on _Google Cloud Platform_.

## Requirements

* _Google Cloud Platform_ account

# Steps

1. Open https://console.cloud.google.com/[GCP console] and create new project:

2. Clone this repository:

```sh
git clone https://gitlab.com/andrzej1_1/hyperflow-k8s-example.git
cd ./hyperflow-k8s-example
```

3. Create infrastructure:
```sh
terraform init
terraform apply -auto-approve -var="gcp_project_id=${DEVSHELL_PROJECT_ID}"
```

4. Setup access to cluster:
```sh
gcloud container clusters get-credentials standard-cluster-2 --zone europe-west4-a --project <PROJECT>
```
5. Setup workflow files:

Download workflow data files:
```sh
wget https://general-bucket-plinta.s3-eu-west-1.amazonaws.com/montage/montage0.25_intermediate.tar.gz
wget https://general-bucket-plinta.s3-eu-west-1.amazonaws.com/montage/montage1.0_intermediate.tar.gz
wget https://general-bucket-plinta.s3-eu-west-1.amazonaws.com/montage/montage2.0_intermediate.tar.gz
tar xvzf *.tar.gz
```
```sh
kubectl wait --for=condition=Ready --selector=name=demo-deployment-hyperflow pod
MAIN_CONTAINER=$(kubectl get pods --selector=name=demo-deployment-hyperflow --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
kubectl cp montage-workflow ${MAIN_CONTAINER}:montage-workflow
kubectl cp montage-data ${MAIN_CONTAINER}:/opt/hfstorage/
kubectl exec ${MAIN_CONTAINER} -- bash -c "mv /opt/hfstorage/montage-data/* /opt/hfstorage"
```

6. Run workflow:

```sh
kubectl exec -it ${MAIN_CONTAINER} -- bash -c "REDIS_URL=\"redis://redis-service:6379\" ./bin/hflow run -s montage-workflow"
```

7. (optional) Destroy whole environment:
```
terraform destroy
```

---

### NOTE

Due to _Terraform_ https://github.com/terraform-providers/terraform-provider-kubernetes/issues/102[issue #102] patched version of provider is used.
